v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 1420 -1600 1420 -1580 { lab=VOUT}
N 1360 -1550 1380 -1550 { lab=VIN}
N 1370 -1550 1370 -1510 { lab=VIN}
N 1360 -1630 1380 -1630 { lab=VC}
N 1420 -1680 1420 -1660 { lab=VPWR}
N 1420 -1630 1430 -1630 { lab=VPWR}
N 1430 -1630 1430 -1550 { lab=VPWR}
N 1420 -1550 1430 -1550 { lab=VPWR}
N 1370 -1510 1520 -1510 { lab=VIN}
N 1520 -1550 1520 -1510 { lab=VIN}
N 1430 -1630 1490 -1630 { lab=VPWR}
N 1490 -1680 1490 -1630 { lab=VPWR}
N 1360 -1680 1490 -1680 { lab=VPWR}
N 1420 -1590 1600 -1590 { lab=VOUT}
N 1560 -1590 1560 -1580 { lab=VOUT}
N 1560 -1520 1560 -1470 { lab=#net1}
N 1560 -1440 1660 -1440 { lab=VGND}
N 1560 -1550 1660 -1550 { lab=VGND}
N 1660 -1550 1660 -1440 { lab=VGND}
N 1520 -1510 1520 -1440 { lab=VIN}
N 1420 -1520 1420 -1300 { lab=VGND}
N 1420 -1300 1560 -1300 { lab=VGND}
N 1370 -1300 1420 -1300 { lab=VGND}
N 1560 -1300 1660 -1300 { lab=VGND}
N 1660 -1440 1660 -1300 { lab=VGND}
N 1560 -1410 1560 -1300 { lab=VGND}
C {xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1400 -1630 0 0 {name=M3
L=0.25
W=1.65
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1400 -1550 0 0 {name=M4
L=0.15
W=9
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1540 -1550 0 0 {name=M6
L=0.25
W=1.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/ipin.sym} 1360 -1550 0 0 {name=p7 lab=VIN}
C {devices/ipin.sym} 1360 -1630 0 0 {name=p8 lab=VC}
C {devices/opin.sym} 1600 -1590 0 0 {name=p4 lab=VOUT}
C {xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1540 -1440 0 0 {name=M1
L=0.25
W=1.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 1360 -1680 0 0 {name=l1 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} 1370 -1300 0 0 {name=l2 sig_type=std_logic lab=VGND}
