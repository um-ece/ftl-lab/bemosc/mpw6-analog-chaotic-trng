v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 2330 -2080 2440 -2080 { lab=VOUT_2}
N 2150 -2080 2160 -2080 { lab=Ini_con}
N 2150 -2060 2160 -2060 { lab=Ini_en}
N 2750 -1890 2880 -1890 { lab=#net1}
N 2670 -1890 2750 -1890 { lab=#net1}
N 2590 -2080 2730 -2080 { lab=#net2}
N 2730 -2080 2870 -2080 { lab=#net2}
N 2370 -2080 2370 -1910 { lab=VOUT_2}
N 2850 -2060 2870 -2060 { lab=en_1}
N 2370 -1890 2440 -1890 { lab=VOUT_2}
N 2370 -1910 2370 -1890 { lab=VOUT_2}
N 2610 -1890 2670 -1890 { lab=#net1}
N 3040 -2080 3080 -2080 { lab=VOUT_1}
N 3080 -2080 3080 -1890 { lab=VOUT_1}
N 3030 -1890 3080 -1890 { lab=VOUT_1}
N 3080 -1980 3120 -1980 { lab=VOUT_1}
N 2370 -1990 2400 -1990 { lab=VOUT_2}
N 2420 -2060 2440 -2060 { lab=Vc_1}
N 3030 -1870 3040 -1870 { lab=Vc_2}
C {devices/lab_pin.sym} 2850 -2060 0 0 {name=l4 sig_type=std_logic lab=en_1}
C {devices/lab_pin.sym} 2610 -1870 0 1 {name=l24 sig_type=std_logic lab=en_2}
C {CLK_GEN.sym} 2340 -2260 0 0 {name=x6 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR}
C {devices/lab_pin.sym} 2490 -2290 0 1 {name=p1 lab=clk1ADV}
C {devices/lab_pin.sym} 2490 -2270 0 1 {name=p3 lab=clk1DEL}
C {devices/lab_pin.sym} 2490 -2250 0 1 {name=p4 lab=clk2DEL}
C {devices/lab_pin.sym} 2490 -2230 0 1 {name=p5 lab=clk2ADV}
C {AnSw.sym} 2260 -2070 0 0 {name=x1 VGND=VGND VPWR=VPWR}
C {AnSw.sym} 2970 -2070 0 0 {name=x2 VGND=VGND VPWR=VPWR}
C {AnSw.sym} 2510 -1880 0 1 {name=x5 VGND=VGND VPWR=VPWR}
C {devices/title.sym} 2160 -1590 0 0 {name=l1 author="Partha"}
C {devices/opin.sym} 3120 -1980 0 0 {name=p6 lab=VOUT_1}
C {devices/opin.sym} 2400 -1990 0 0 {name=p7 lab=VOUT_2}
C {MAP_T05.sym} 2530 -2070 0 0 {name=x3 VGND=VGND VPWR=VPWR}
C {MAP_T05.sym} 2940 -1880 0 1 {name=x4 VGND=VGND VPWR=VPWR}
C {devices/ipin.sym} 2150 -2080 0 0 {name=p12 lab=Ini_con}
C {devices/ipin.sym} 2150 -2060 0 0 {name=p13 lab=Ini_en}
C {devices/ipin.sym} 2190 -2290 0 0 {name=p14 lab=ref_clk}
C {devices/ipin.sym} 2420 -2060 0 0 {name=p2 lab=Vc_1}
C {devices/ipin.sym} 3040 -1870 0 1 {name=p8 lab=Vc_2}
C {LB.sym} 2700 -2210 0 0 {name=x9}
C {devices/lab_pin.sym} 2820 -2300 0 1 {name=l2 sig_type=std_logic lab=en_1}
C {devices/lab_pin.sym} 2820 -2280 0 1 {name=l3 sig_type=std_logic lab=en_2}
C {devices/lab_pin.sym} 2680 -2300 2 1 {name=l5 sig_type=std_logic lab=Ini_en}
C {devices/lab_pin.sym} 2680 -2280 0 0 {name=p9 lab=clk1ADV}
C {devices/lab_pin.sym} 2680 -2260 0 0 {name=p10 lab=clk2ADV}
C {devices/lab_pin.sym} 2680 -2240 0 0 {name=p11 lab=VGND}
C {devices/lab_pin.sym} 2680 -2220 0 0 {name=p15 lab=VPWR}
C {devices/ipin.sym} 2140 -2150 0 0 {name=p16 lab=VPWR}
C {devices/ipin.sym} 2140 -2120 0 0 {name=p17 lab=VGND}
