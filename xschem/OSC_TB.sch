v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -490 -150 -490 -130 { lab=GND}
N -490 -240 -490 -210 { lab=VPWR}
N -420 -150 -420 -130 { lab=GND}
N -420 -240 -420 -210 { lab=VGND}
N -350 -150 -350 -130 { lab=GND}
N -350 -240 -350 -210 { lab=VC}
N -280 -150 -280 -130 { lab=GND}
N -280 -240 -280 -210 { lab=Ini_con}
N -810 -150 -810 -120 { lab=GND}
N -1080 -150 -1080 -130 { lab=GND}
N -1080 -240 -1080 -210 { lab=Ini_en}
N -810 -240 -810 -210 { lab=ref}
C {devices/vsource.sym} -490 -180 0 0 {name=V2 value=1.8}
C {devices/lab_pin.sym} -490 -240 1 0 {name=l6 sig_type=std_logic lab=VPWR}
C {devices/gnd.sym} -490 -130 0 0 {name=l7 lab=GND}
C {devices/vsource.sym} -420 -180 0 0 {name=V1 value=0}
C {devices/lab_pin.sym} -420 -240 1 0 {name=l11 sig_type=std_logic lab=VGND}
C {devices/gnd.sym} -420 -130 0 0 {name=l12 lab=GND}
C {devices/vsource.sym} -350 -180 0 0 {name=V6 value=0.54}
C {devices/lab_pin.sym} -350 -240 1 0 {name=l10 sig_type=std_logic lab=VC}
C {devices/gnd.sym} -350 -130 0 0 {name=l18 lab=GND}
C {devices/vsource.sym} -280 -180 0 0 {name=V8 value=0.33}
C {devices/lab_pin.sym} -280 -240 1 0 {name=l31 sig_type=std_logic lab=Ini_con}
C {devices/gnd.sym} -280 -130 0 0 {name=l32 lab=GND}
C {devices/vsource.sym} -810 -180 0 0 {name=V12 value="PULSE (0 1.8 2.4NS 2PS 2PS 20NS 40NS)"}
C {devices/gnd.sym} -810 -120 0 0 {name=l44 lab=GND}
C {devices/vsource.sym} -1080 -180 0 0 {name=V13 value="PULSE (0 1.8 0 2PS 2PS 43NS 160000NS)"}
C {devices/lab_pin.sym} -1080 -240 1 0 {name=l67 sig_type=std_logic lab=Ini_en}
C {devices/gnd.sym} -1080 -130 0 0 {name=l68 lab=GND}
C {devices/lab_pin.sym} -810 -240 1 0 {name=l69 sig_type=std_logic lab=ref}
C {devices/code.sym} 280 -140 0 0 {name=TT_MODELS
only_toplevel=false
format="tcleval( @value )"
value="
.lib \\\\$::SKYWATER_MODELS\\\\/sky130.lib.spice tt *Use this statement to import primitives
.include \\\\$::SKYWATER_STDCELLS\\\\/sky130_fd_sc_hd.spice *Use this statement to import STD Cells
"
}
C {devices/title.sym} -1000 490 0 0 {name=l1 author="Partha"}
C {devices/code_shown.sym} -100 -210 0 0 {name=NGSPICE only_toplevel=false 
value="
.title Testbench for Xschem TRNG OSCILLATOR
parameter sweep
.control
*.options CONVSTEP=10n
*.options INTERP
set wr_singlescale
*set wr_vecnames
set appendwrite

let start_VC = 0.500
let stop_VC= 0.505
let delta_VC=0.005
let VC_act= start_VC

*loop
while VC_act le stop_VC
alter V6 VC_act

tran 0.1n 160n
*tran 20n 160000n
plot VOUT_2

let VC_act = VC_act + delta_VC

*wrdata T05_G5_non_ideal_clk.txt VC, VOUT_2
*wrdata T05_G8_non_ideal_clk_interp_newSW.txt VC, VOUT_1, VOUT_2

end

save all
.endc
.end
"}
C {OSC.sym} -650 180 0 0 {name=x2}
C {devices/lab_pin.sym} -670 70 0 0 {name=l2 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} -670 90 0 0 {name=l3 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} -670 110 0 0 {name=p9 lab=Ini_con}
C {devices/lab_pin.sym} -670 130 0 0 {name=p10 lab=VC}
C {devices/lab_pin.sym} -670 170 0 0 {name=p12 lab=VC}
C {devices/lab_pin.sym} -670 150 0 0 {name=p11 lab=Ini_en}
C {devices/lab_pin.sym} -530 50 0 1 {name=p13 lab=VOUT_2}
C {devices/lab_pin.sym} -530 70 0 1 {name=p14 lab=VOUT_1}
C {devices/lab_pin.sym} -670 50 0 0 {name=l4 sig_type=std_logic lab=ref
}
