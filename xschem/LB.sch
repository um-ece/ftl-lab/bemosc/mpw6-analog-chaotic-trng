v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 3200 -1340 3220 -1340 { lab=#net1}
N 3070 -1320 3080 -1320 { lab=Ini_en_inv}
N 3300 -1340 3320 -1340 { lab=en_1}
N 3070 -1360 3080 -1360 { lab=clk1ADV}
N 3200 -1240 3220 -1240 { lab=#net2}
N 3070 -1220 3080 -1220 { lab=Ini_en_inv}
N 3300 -1240 3320 -1240 { lab=en_2}
N 3070 -1260 3080 -1260 { lab=clk2ADV}
N 2610 -1280 2640 -1280 { lab=Ini_en}
N 2720 -1280 2860 -1280 { lab=Ini_en_inv}
N 2390 -1290 2410 -1290 { lab=VGND}
N 2390 -1250 2410 -1250 { lab=VPWR}
C {xschem_sky130/sky130_stdcells/nand2_1.sym} 3140 -1340 0 0 {name=x1 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 3260 -1340 0 0 {name=x2 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {devices/lab_pin.sym} 3070 -1320 2 1 {name=l1 sig_type=std_logic lab=Ini_en_inv}
C {devices/lab_pin.sym} 3320 -1340 0 1 {name=l2 sig_type=std_logic lab=en_1}
C {devices/lab_pin.sym} 3070 -1360 2 1 {name=l3 sig_type=std_logic lab=clk1ADV}
C {xschem_sky130/sky130_stdcells/nand2_1.sym} 3140 -1240 0 0 {name=x3 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {devices/lab_pin.sym} 3070 -1220 2 1 {name=l4 sig_type=std_logic lab=Ini_en_inv}
C {devices/lab_pin.sym} 3320 -1240 0 1 {name=l5 sig_type=std_logic lab=en_2}
C {devices/lab_pin.sym} 3070 -1260 2 1 {name=l8 sig_type=std_logic lab=clk2ADV}
C {devices/lab_pin.sym} 2610 -1280 2 1 {name=l65 sig_type=std_logic lab=Ini_en}
C {devices/lab_pin.sym} 2860 -1280 2 0 {name=l66 sig_type=std_logic lab=Ini_en_inv}
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 3260 -1240 0 0 {name=x4 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 2680 -1280 0 0 {name=x8 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {devices/ipin.sym} 2390 -1380 0 0 {name=p8 lab=Ini_en}
C {devices/ipin.sym} 2390 -1350 0 0 {name=p1 lab=clk1ADV}
C {devices/ipin.sym} 2390 -1320 0 0 {name=p2 lab=clk2ADV}
C {devices/ipin.sym} 2390 -1290 0 0 {name=p5 lab=VGND}
C {devices/ipin.sym} 2390 -1250 0 0 {name=p6 lab=VPWR}
C {devices/lab_pin.sym} 2410 -1290 2 0 {name=l6 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 2410 -1250 2 0 {name=l7 sig_type=std_logic lab=VPWR}
C {devices/opin.sym} 2380 -1170 0 0 {name=p3 lab=en_2}
C {devices/opin.sym} 2380 -1200 0 0 {name=p4 lab=en_1}
