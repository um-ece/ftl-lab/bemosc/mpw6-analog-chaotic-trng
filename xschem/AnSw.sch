v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 1350 -1700 1370 -1700 { lab=VIN}
N 1350 -1700 1350 -1540 { lab=VIN}
N 1350 -1510 1370 -1510 { lab=VIN}
N 1520 -1770 1520 -1740 { lab=#net1}
N 1550 -1700 1570 -1700 { lab=VOUT}
N 1570 -1700 1570 -1630 { lab=VOUT}
N 1570 -1630 1570 -1540 { lab=VOUT}
N 1550 -1510 1570 -1510 { lab=VOUT}
N 1520 -1580 1520 -1510 { lab=VPB}
N 1430 -1700 1490 -1700 { lab=VOUT}
N 1430 -1510 1490 -1510 { lab=VOUT}
N 1400 -1470 1400 -1450 { lab=en}
N 1520 -1470 1520 -1450 { lab=en}
N 1520 -1700 1520 -1630 { lab=VNB}
N 1400 -1580 1400 -1510 { lab=VNB}
N 1400 -1700 1400 -1630 { lab=VPB}
N 1480 -1700 1480 -1670 { lab=VOUT}
N 1480 -1670 1570 -1670 { lab=VOUT}
N 1480 -1540 1480 -1510 { lab=VOUT}
N 1480 -1540 1570 -1540 { lab=VOUT}
N 1180 -1570 1190 -1570 { lab=en}
N 1270 -1570 1290 -1570 { lab=#net1}
N 1150 -1570 1180 -1570 { lab=en}
N 1170 -1570 1170 -1450 { lab=en}
N 1350 -1540 1350 -1510 { lab=VIN}
N 1570 -1540 1570 -1510 { lab=VOUT}
N 1570 -1620 1610 -1620 { lab=VOUT}
N 1860 -1660 1860 -1620 { lab=VPB}
N 1780 -1660 1780 -1620 { lab=VNB}
N 1350 -1890 1370 -1890 { lab=VIN}
N 1350 -1890 1350 -1700 { lab=VIN}
N 1430 -1890 1440 -1890 { lab=VOUT}
N 1440 -1890 1440 -1700 { lab=VOUT}
N 1400 -1890 1400 -1800 { lab=VPB}
N 1400 -1950 1400 -1930 { lab=#net1}
N 1290 -1950 1290 -1570 { lab=#net1}
N 1290 -1950 1400 -1950 { lab=#net1}
N 1400 -1950 1520 -1950 { lab=#net1}
N 1520 -1950 1520 -1770 { lab=#net1}
N 1290 -1760 1400 -1760 { lab=#net1}
N 1400 -1760 1400 -1740 { lab=#net1}
N 1520 -1400 1520 -1320 { lab=VPB}
N 1470 -1320 1490 -1320 { lab=VOUT}
N 1470 -1510 1470 -1320 { lab=VOUT}
N 1550 -1320 1570 -1320 { lab=VOUT}
N 1570 -1510 1570 -1320 { lab=VOUT}
N 1520 -1280 1520 -1260 { lab=en}
N 1400 -1260 1520 -1260 { lab=en}
N 1400 -1450 1400 -1260 { lab=en}
N 1170 -1450 1520 -1450 { lab=en}
C {xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1400 -1490 3 0 {name=M3
L=0.15
W=1.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1400 -1720 3 1 {name=M1
L=0.15
W=1.65
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 1400 -1630 0 0 {name=l2 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 1400 -1580 0 0 {name=l9 sig_type=std_logic lab=VNB}
C {xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1520 -1490 1 1 {name=M2
L=0.15
W=1.65
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {xschem_sky130/sky130_fd_pr/nfet_01v8.sym} 1520 -1720 1 0 {name=M4
L=0.15
W=1.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 1520 -1580 2 0 {name=l19 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 1520 -1630 2 0 {name=l20 sig_type=std_logic lab=VNB}
C {devices/ipin.sym} 1150 -1570 0 0 {name=p4 lab=en}
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 1230 -1570 0 0 {name=x2 VGND=VGND VNB=VNB VPB=VPB VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {devices/ipin.sym} 1350 -1650 0 0 {name=p5 lab=VIN}
C {devices/opin.sym} 1610 -1620 0 0 {name=p6 lab=VOUT}
C {devices/vsource.sym} 1860 -1590 0 0 {name=V1 value=1.8}
C {devices/gnd.sym} 1860 -1560 0 0 {name=l11 lab=GND}
C {devices/lab_pin.sym} 1860 -1660 0 0 {name=l12 sig_type=std_logic lab=VPB}
C {devices/vsource.sym} 1780 -1590 0 0 {name=V6 value=0}
C {devices/gnd.sym} 1780 -1560 0 0 {name=l13 lab=GND}
C {devices/lab_pin.sym} 1780 -1660 0 0 {name=l14 sig_type=std_logic lab=VNB}
C {xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1400 -1910 3 1 {name=M5
L=0.15
W=1.65
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 1400 -1800 0 0 {name=l1 sig_type=std_logic lab=VPB}
C {xschem_sky130/sky130_fd_pr/pfet_01v8.sym} 1520 -1300 1 1 {name=M6
L=0.15
W=1.65
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 1520 -1400 2 0 {name=l3 sig_type=std_logic lab=VPB}
C {devices/title.sym} 960 -1220 0 0 {name=l4 author="Partha"}
