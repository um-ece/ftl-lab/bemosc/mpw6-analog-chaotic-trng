v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
T {Est. Non-Overlap window is ~.41 ns using clk 1/2 del signals} -420 -350 0 0 0.4 0.4 {}
N -230 -130 -210 -130 { lab=#net1}
N -130 -130 -110 -130 { lab=#net2}
N -30 -130 -10 -130 { lab=#net3}
N 190 -130 230 -130 { lab=#net4}
N 310 -130 350 -130 { lab=clk1DEL}
N 210 -130 210 -50 { lab=#net4}
N 70 -130 110 -130 { lab=clk1ADV}
N 90 -170 90 -130 { lab=clk1ADV}
N 90 -170 350 -170 { lab=clk1ADV}
N -230 110 -210 110 { lab=#net5}
N -130 110 -110 110 { lab=#net6}
N -30 110 -10 110 { lab=#net7}
N 190 110 230 110 { lab=#net8}
N 310 110 350 110 { lab=clk2DEL}
N 70 110 110 110 { lab=clk2ADV}
N 90 110 90 150 { lab=clk2ADV}
N 90 150 350 150 { lab=clk2ADV}
N 210 30 210 110 { lab=#net8}
N -310 -70 -310 30 { lab=#net9}
N -370 -70 -310 -70 { lab=#net9}
N -370 -110 -370 -70 { lab=#net9}
N -370 -110 -350 -110 { lab=#net9}
N -330 -50 -330 50 { lab=#net10}
N -370 50 -330 50 { lab=#net10}
N -370 50 -370 90 { lab=#net10}
N -370 90 -350 90 { lab=#net10}
N -410 130 -350 130 { lab=#net11}
N -410 -150 -350 -150 { lab=ref}
N -410 -150 -410 -50 { lab=ref}
N -410 30 -410 130 { lab=#net11}
N -470 -150 -410 -150 { lab=ref}
N -330 -50 -270 -50 { lab=#net10}
N -310 30 -270 30 { lab=#net9}
C {devices/opin.sym} 340 -170 0 0 {name=p1 lab=clk1ADV}
C {devices/opin.sym} 340 -130 0 0 {name=p2 lab=clk1DEL}
C {devices/opin.sym} 340 150 0 0 {name=p3 lab=clk2ADV}
C {devices/opin.sym} 340 110 0 0 {name=p4 lab=clk2DEL}
C {devices/ipin.sym} -470 -150 0 0 {name=p5 lab=ref}
C {devices/title.sym} -430 330 0 0 {name=l1 author="Parker Hardy"}
C {xschem_sky130/sky130_stdcells/nand2_1.sym} -290 -130 0 0 {name=x2 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/nand2_1.sym} -290 110 0 0 {name=x8 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {devices/code.sym} 470 -30 0 0 {name=Models
only_toplevel=false
verilog_ignore = true
format="tcleval( @value )"
value="
.include \\\\$::SKYWATER_STDCELLS\\\\/sky130_fd_sc_hd.spice
"
}
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -170 -130 0 0 {name=x1 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -70 -130 0 0 {name=x3 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 30 -130 0 0 {name=x4 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 150 -130 0 0 {name=x5 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 270 -130 0 0 {name=x6 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -170 110 0 0 {name=x7 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -70 110 0 0 {name=x9 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 30 110 0 0 {name=x10 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 150 110 0 0 {name=x11 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 270 110 0 0 {name=x12 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -410 -10 1 0 {name=x13 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -230 -50 2 0 {name=x14 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -150 -50 2 0 {name=x15 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -70 -50 2 0 {name=x16 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 10 -50 2 0 {name=x17 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 90 -50 2 0 {name=x18 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 170 -50 2 0 {name=x19 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -230 30 2 0 {name=x20 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -150 30 2 0 {name=x21 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} -70 30 2 0 {name=x22 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 10 30 2 0 {name=x23 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 90 30 2 0 {name=x24 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 170 30 2 0 {name=x25 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
