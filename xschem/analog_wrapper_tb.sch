v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 300 -210 400 -210 { lab=#net1}
N 300 -270 400 -270 { lab=#net2}
N 300 -230 400 -230 { lab=#net3}
N 300 -190 400 -190 { lab=#net4}
N 300 -190 400 -190 { lab=#net4}
N 300 -170 400 -170 { lab=#net5}
N 300 -130 400 -130 { lab=#net6}
N 300 -110 400 -110 { lab=#net7}
N 300 -90 400 -90 { lab=#net8}
N 300 -10 400 -10 { lab=#net9}
N 300 70 400 70 { lab=#net10}
N -60 -290 -0 -290 { lab=#net11}
N -60 -270 0 -270 { lab=#net12}
N -60 -250 0 -250 { lab=#net13}
N -60 -230 0 -230 { lab=#net14}
N -60 -210 0 -210 { lab=#net15}
N -60 -210 0 -210 { lab=#net15}
N -60 -190 0 -190 { lab=#net16}
N -60 -190 0 -190 { lab=#net16}
N -60 -170 0 -170 { lab=#net17}
N -60 -150 0 -150 { lab=#net18}
N -60 -130 0 -130 { lab=#net19}
N -60 -110 0 -110 { lab=#net20}
N -60 -90 0 -90 { lab=#net21}
N -60 -70 0 -70 { lab=#net22}
N -60 -50 0 -50 { lab=user_clock2}
N 300 -70 400 -70 { lab=#net23}
N 300 -50 400 -50 { lab=#net24}
N 300 -30 400 -30 { lab=#net25}
N 300 -250 400 -250 { lab=GND}
N 300 -150 400 -150 { lab=#net26}
N 300 10 400 10 { lab=io_analog[10:0]}
N 300 30 400 30 { lab=#net27}
N 300 50 400 50 { lab=#net28}
N 300 -290 400 -290 { lab=#net29}
N 400 -250 460 -250 { lab=GND}
N 460 -250 460 -220 { lab=GND}
N 400 -210 440 -210 { lab=#net1}
N 440 -210 440 -180 { lab=#net1}
N 20 -390 20 -370 { lab=GND}
N 20 -510 20 -450 { lab=io_analog[3]}
N 620 -310 620 -280 { lab=GND}
N 620 -400 620 -370 { lab=user_clock2}
N 570 -220 570 -200 { lab=GND}
N 570 -310 570 -280 { lab=io_analog[5]}
N 520 -140 520 -120 { lab=GND}
N 520 -230 520 -200 { lab=io_analog[1]}
C {user_analog_project_wrapper.sym} 150 -110 0 0 {name=x1}
C {devices/gnd.sym} 460 -220 0 0 {name=l2 lab=GND}
C {devices/code.sym} 920 -130 0 0 {name=TT_MODELS only_toplevel=false
format="tcleval(@value )" value=".lib \\\\$::SKYWATER_MODELS\\\\/sky130.lib.spice tt
.include \\\\$::SKYWATER_STDCELLS\\\\/sky130_fd_sc_hd.spice"}
C {devices/code_shown.sym} 1100 -130 0 0 {name=s1
only_toplevel=false
value="
.title Testbench for MPW6 Wrapper+TRNG 
.control
let start_VC = 0.500
let stop_VC= 0.505
let delta_VC=0.005
let VC_act= start_VC

*while VC_act le stop_VC
alter V6 VC_act

tran 0.1n 160n

plot V(\\"io_analog[2]\\") 
plot V(\\"io_analog[4]\\")
.endc"}
C {devices/vsource.sym} 440 -150 0 0 {name=V1 value=1.8}
C {devices/gnd.sym} 440 -120 0 0 {name=l3 lab=GND}
C {devices/vsource.sym} 20 -420 0 0 {name=V6 value=0.54}
C {devices/gnd.sym} 20 -370 0 0 {name=l18 lab=GND}
C {devices/lab_pin.sym} 20 -510 0 0 {name=l1 sig_type=std_logic lab=io_analog[3]}
C {devices/vsource.sym} 620 -340 0 0 {name=V12 value="PULSE (0 1.8 2.4NS 2PS 2PS 20NS 40NS)"}
C {devices/gnd.sym} 620 -280 0 0 {name=l44 lab=GND}
C {devices/lab_pin.sym} 620 -400 1 0 {name=l69 sig_type=std_logic lab=user_clock2}
C {devices/vsource.sym} 570 -250 0 0 {name=V13 value="PULSE (0 1.8 0 2PS 2PS 43NS 160000NS)"}
C {devices/lab_pin.sym} 570 -310 1 0 {name=l67 sig_type=std_logic lab=io_analog[5]}
C {devices/gnd.sym} 570 -200 0 0 {name=l68 lab=GND}
C {devices/vsource.sym} 520 -170 0 0 {name=V8 value=0.33}
C {devices/lab_pin.sym} 520 -230 1 0 {name=l31 sig_type=std_logic lab=io_analog[1]}
C {devices/gnd.sym} 520 -120 0 0 {name=l32 lab=GND}
C {devices/lab_pin.sym} 400 10 2 0 {name=l5 sig_type=std_logic lab=io_analog[10:0]}
C {devices/lab_pin.sym} -60 -50 0 0 {name=l7 sig_type=std_logic lab=user_clock2}
