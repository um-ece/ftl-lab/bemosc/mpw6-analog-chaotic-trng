# Caravel Analog User

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![CI](https://github.com/efabless/caravel_user_project_analog/actions/workflows/user_project_ci.yml/badge.svg)](https://github.com/efabless/caravel_user_project_analog/actions/workflows/user_project_ci.yml) [![Caravan Build](https://github.com/efabless/caravel_user_project_analog/actions/workflows/caravan_build.yml/badge.svg)](https://github.com/efabless/caravel_user_project_analog/actions/workflows/caravan_build.yml)

---

| :exclamation: Important Note            |
|-----------------------------------------|

## Please fill in your project documentation in this README.md file 
This repo is the submission for the google-skywater open MPW program. This is the submission for MPW6 and consists of an analog implementation of a chaos based TRNG.
The design is the work of the BeMOSC group at the University of Mississippi, and has theoretical applications in the field of cryptography and cyber security.

:warning: | Use this sample project for analog user projects. 
:---: | :---

---

Refer to [README](docs/source/index.rst) for this sample project documentation. 
